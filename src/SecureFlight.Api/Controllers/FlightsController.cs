﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class FlightsController(
    IService<Flight> flightService,
    IService<Passenger> passengerService,
    IRepository<Flight> flightRepository,
    IMapper mapper)
    : SecureFlightBaseController(mapper)
{
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get()
    {
        var flights = await flightService.GetAllAsync();
        return MapResultToDataTransferObject<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }

    [HttpPost("add/{flightId}/passenger/{passengerId}")]
    [ProducesResponseType(typeof(FlightDataTransferObject), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Post([FromRoute] long flightId, [FromRoute] string passengerId)
    {
        var flightTask = await flightService.FindAsync(flightId);
        var passengerTask = await passengerService.FindAsync(passengerId);
        var flight = flightTask.Result;
        var passenger = passengerTask.Result;


        if(flight is null)
        {
            throw new Exception("Flight not found");
        }

        if(passenger is null)
        {
            throw new Exception("Passenger not found");
        }

        flight.Passengers.Add(passenger);
        await flightRepository.Update(flight);

        return MapResultToDataTransferObject<Flight, FlightDataTransferObject>(flight);

    }

    [HttpDelete("remove/{flightId}/passenger/{passengerId}")]
    [ProducesResponseType(typeof(FlightDataTransferObject), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Delete([FromRoute] long flightId, [FromRoute] string passengerId)
    {
        var allFlightsTask = await flightService.GetAllAsync();
        var flightTask = await flightService.FindAsync(flightId);
        var passengerTask = await passengerService.FindAsync(passengerId);

        var flight = flightTask.Result;
        var passenger = passengerTask.Result;
        var allFlights = allFlightsTask.Result;


        if (flight is null)
        {
            throw new Exception("Flight not found");
        }

        if (passenger is null)
        {
            throw new Exception("Passenger not found");
        }

        flight.Passengers.Remove(passenger);
        await flightRepository.Update(flight);

        var isOnOtherFlight = allFlights.Any(x => x.Passengers.Any(x => x.Id == passengerId));
        if(!isOnOtherFlight)
        {
            await passengerService.Delete(passenger);
        }
       

        // check if the passenger has any other flight


        return MapResultToDataTransferObject<Flight, FlightDataTransferObject>(flight);

    }
}